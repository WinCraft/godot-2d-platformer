extends KinematicBody2D

var velocity = Vector2(0,0)
const SPEED = 180
const GRAVITY = 30
const JUMPFORCE = 1100

func _physics_process(delta):
	if Input.is_action_pressed("ui_left"):
		velocity.x = -SPEED
		$sprite.play("walk")
		$sprite.flip_h = true
	elif Input.is_action_pressed("ui_right"):
		velocity.x = SPEED
		$sprite.play("walk")
		$sprite.flip_h = false
	else:
		$sprite.play("idle")
	
	if not is_on_floor():
		$sprite.play("jump")
	
	velocity.y += GRAVITY
	if Input.is_action_just_pressed("ui_select") and is_on_floor():
		velocity.y += -JUMPFORCE
		$sprite.play("jump")
	
	velocity = move_and_slide(velocity, Vector2.UP)
	
	velocity.x = lerp(velocity.x, 0, 0.05)
